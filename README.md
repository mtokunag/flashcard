This is a FlashCard Android App that can store FlashCards for multiple Subjects.

There are two ways of reading FlashCards, either swiping or using Next/Previous Buttons.

One can create/read/update/delete flashcards and subjects.

One should be able to copy this repository and open it with Android Studio and get the app working correctly.