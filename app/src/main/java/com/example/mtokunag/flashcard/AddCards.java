package com.example.mtokunag.flashcard;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.UUID;

/**
 * Created by mtokunag on 12/14/2015.
 */
public class AddCards extends Activity {
    public static final String EXTRA_CARD_ID="com.example.mtokunag.flashcard.card_id";
    public static final String CARD_ADD="card added";
    Subject sc;
    TextView add_word;
    TextView add_definition;
    Button save_button;
    FlashCard fc;
    boolean isNew;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        setContentView(R.layout.add_new_card);
        UUID subjectId = (UUID)getIntent().getSerializableExtra(MainActivity.SUBJECT_ID);
        sc = Cards.get(this).getSubject(subjectId);

        UUID cardId = (UUID)getIntent().getSerializableExtra(EXTRA_CARD_ID);
        fc = Cards.get(this).getCard(sc, cardId);

        // remove card if word and definition are both null.
        // Otherwise bring up already existing flashcard
        if(fc.getWord()==null && fc.getDefinition()==null) {
            Cards.get(this).deleteCard(sc, fc);
            isNew = true;
        }
        else
            isNew = false;

        add_word = (TextView)findViewById(R.id.add_word);
        if(!isNew){
            add_word.setText(fc.getWord());
        }
        add_word.addTextChangedListener(new TextWatcher(){
            public void onTextChanged(CharSequence c, int start, int before, int count) {
               fc.setWord(c.toString());
            }

            public void beforeTextChanged(CharSequence c, int start, int count, int after) {
                // Intentionally left blank
            }

            public void afterTextChanged(Editable c) {
                // This one too.
            }
        });

        add_definition = (TextView)findViewById(R.id.add_definition);
        if(!isNew){
            add_definition.setText(fc.getDefinition());
        }
        add_definition.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence c, int start, int before, int count) {
                fc.setDefinition(c.toString());
            }

            public void beforeTextChanged(CharSequence c, int start, int count, int after) {
                // Intentionally left blank
            }

            public void afterTextChanged(Editable c) {
                // This one too.
            }
        });

        add_definition = (TextView)findViewById(R.id.add_definition);

        save_button = (Button)findViewById(R.id.save_flashcard);
        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNew && (fc.getWord()!=null || fc.getDefinition()!=null) ) {
                    Cards.get(AddCards.this).addCard(sc, fc);
                }
                Cards.get(AddCards.this).saveCards();
                finish();
            }
        });

    }





}
