package com.example.mtokunag.flashcard;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by mtokunag on 12/22/2015.
 */
public class AddSubject extends Activity {
    public static final String EXTRA_SUBJECT_ID="com.example.mtokunag.flashcard.subject_id";
    TextView add_subject;
    Button save_subject_button;
    Subject sc;
    boolean isNew;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_new_subject);

        UUID subjectId = (UUID)getIntent().getSerializableExtra(EXTRA_SUBJECT_ID);
        sc = Cards.get(this).getSubject(subjectId);

        if(sc.getSubject()==null) {
            Cards.get(this).deleteSubject(sc);
            isNew = true;
        }
        else{
            isNew=false;
        }

        add_subject = (TextView)findViewById(R.id.add_subject);
        if(!isNew){
            add_subject.setText(sc.getSubject());
        }
        add_subject.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence c, int start, int before, int count) {
                sc.setSubject(c.toString());
            }

            public void beforeTextChanged(CharSequence c, int start, int count, int after) {
                // Intentionally left blank
            }

            public void afterTextChanged(Editable c) {
                // This one too.
            }
        });


        save_subject_button = (Button)findViewById(R.id.save_subject);
        save_subject_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNew && (sc.getSubject()!=null)) {
                    ArrayList<FlashCard> fc = new ArrayList<FlashCard>();
                    sc.setFlashCards(fc);
                    Cards.get(AddSubject.this).addSubjectCard(sc);
                }
                Cards.get(AddSubject.this).saveCards();
                finish();
            }
        });


    }
}
