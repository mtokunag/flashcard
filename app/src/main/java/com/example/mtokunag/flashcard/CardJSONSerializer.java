package com.example.mtokunag.flashcard;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONTokener;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;

/**
 * Created by mtokunag on 12/15/2015.
 */
public class CardJSONSerializer {
    private Context mContext;
    private String mFilename;

    public CardJSONSerializer(Context c, String f){
        mContext = c;
        mFilename = f;
    }

    public ArrayList<Subject> loadCards() throws IOException, JSONException {
       // ArrayList<FlashCard> cards = new ArrayList<FlashCard>();
        ArrayList<Subject> subjectCards = new ArrayList<Subject>();
        BufferedReader reader = null;
        try{
            // Open and read the file into a StringBuilder
            InputStream in = mContext.openFileInput(mFilename);
            reader = new BufferedReader(new InputStreamReader(in));
            StringBuilder jsonString = new StringBuilder();
            String line = null;
            while((line=reader.readLine())!=null){
                // Line breaks are omitted and irrelevant
                jsonString.append(line);
            }
            //Parse the JSON using JSONTokener
            JSONArray array = (JSONArray) new JSONTokener(jsonString.toString()).nextValue();

            //Build the array of cards from JSONObjects
            for(int i=0; i<array.length(); i++){
               // cards.add(new FlashCard(array.getJSONObject(i)));
                subjectCards.add(new Subject(array.getJSONObject(i)));
            }
        }
        catch(FileNotFoundException e){

        }
        finally{
            if(reader !=null)
                reader.close();
        }
        return subjectCards;
    }

    public void saveCards(ArrayList<Subject> subject) throws JSONException, IOException{
        JSONArray array =  new JSONArray();
        for(Subject sc: subject){
            array.put(sc.toJSON());
        }

        /*for(FlashCard fc: cards){
            array.put(fc.toJSON());
        }*/

        Writer writer = null;
        try{
            OutputStream out = mContext.openFileOutput(mFilename,Context.MODE_PRIVATE);
            writer = new OutputStreamWriter(out);
            writer.write(array.toString());
        }
        finally{
            if(writer!=null){
                writer.close();
            }
        }
    }
}
