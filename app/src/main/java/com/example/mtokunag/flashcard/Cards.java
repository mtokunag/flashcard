package com.example.mtokunag.flashcard;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by mtokunag on 12/14/2015.
 */
public class Cards {
    private ArrayList<FlashCard> flashCards = null;
    private ArrayList<Subject> subjectCards = null;
    private Context mAppContext;
    private static Cards sCards;

    private static final String TAG = "Cards";
    private static final String FILENAME = "cards.json";
    private CardJSONSerializer mSerializer;

    private Cards(Context appContext){
        mAppContext = appContext;
        mSerializer = new CardJSONSerializer(mAppContext, FILENAME);

        //temp
        //subjectCards = new ArrayList<Subject>();


        try{
            subjectCards = mSerializer.loadCards();
            //flashCards.add(new FlashCard("filler","filler"));
           //Log.v(TAG,"Successfully lodaded cards; SIZE: "+ flashCards.size());
        }
        catch(Exception e){
            subjectCards = new ArrayList<Subject>();
            //subjectCards.add(new FlashCard("filler", "filler"));
            Log.e(TAG,"Error loading cards: ", e);
        }
    }

    public static Cards get(Context c){
        if(sCards == null){
            sCards = new Cards(c.getApplicationContext());
        }
        return sCards;
    }

    // methods handling Subject
    public ArrayList<Subject> getAllCards(){
        return subjectCards;
    }

    public void addSubjectCard(Subject sc){
        subjectCards.add(sc);
    }

    public Subject getSubject(UUID id){
        for(Subject sc: subjectCards){
            if(sc.getId().equals(id))
                return sc;
        }
        return null;
    }

    public void deleteSubject(Subject sc){
        ArrayList<FlashCard> fc_tmp = sc.getFlashCards();
        if(fc_tmp !=null) {
            fc_tmp.clear();
        }
        boolean didItDelete = subjectCards.remove(sc);
        Log.v("DIDITDELETE?:",""+didItDelete);
        Log.v("SIZE:", "" + subjectCards.size());
    }


    // methods handling FlashCards
    public void addCard(Subject sc, FlashCard fc){
        flashCards = sc.getFlashCards();
        flashCards.add(fc);
    }

    public ArrayList<FlashCard> getCards(Subject s){
        return s.getFlashCards();
    }

    public FlashCard getCard(Subject sc, UUID id){
        flashCards = sc.getFlashCards();
        for (FlashCard fc : flashCards) {
            if (fc.getId().equals(id))
                return fc;
        }
        return null;
    }

    public void deleteCard(Subject sc, FlashCard fc){
        flashCards = sc.getFlashCards();
        boolean didItDelete = flashCards.remove(fc);
        Log.v("DIDITDELETE?:",""+didItDelete);
        Log.v("SIZE:", "" + flashCards.size());
    }
    public void remove(Subject sc, int count){
        flashCards.remove(count);
    }

    public boolean saveCards(){
        try{
            mSerializer.saveCards(subjectCards);
            Log.d(TAG, "cards saved to file");
            return true;
        }
        catch(Exception e){
            Log.e(TAG,"Error saving cards:" +e);
            return false;
        }
    }

}
