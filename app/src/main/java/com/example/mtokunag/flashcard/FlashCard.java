package com.example.mtokunag.flashcard;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by mtokunag on 12/14/2015.
 */
public class FlashCard implements Serializable{
    public static final String JSON_ID = "id";
    public static final String JSON_WORD = "word";
    public static final String JSON_DEFINITION = "definition";
    public static boolean showWord = true;
    public static boolean showDef = false;
    private String word;
    private String definition;
    private UUID id;

    public FlashCard(){
        id = UUID.randomUUID();
    }

    public FlashCard(String w, String d){
        id = UUID.randomUUID();
        word = w;
        definition = d;
    }

    public FlashCard(JSONObject json) throws JSONException{
        //Log.v("JSON FC_CONST: ",json.toString());
       // Log.v("JSON FC_CONST_id: ",json.getString(JSON_ID));
        id = UUID.fromString(json.getString(JSON_ID));
        if(json.has(JSON_WORD)){
            //Log.v("JSON FC_CONST_word: ",json.getString(JSON_WORD));
            word = json.getString(JSON_WORD);
        }
        if(json.has(JSON_DEFINITION)){
            //Log.v("JSON FC_CONST_def: ",json.getString(JSON_DEFINITION));
            definition = json.getString(JSON_DEFINITION);
        }
    }
    public JSONObject toJSON() throws JSONException{
        JSONObject json = new JSONObject();
        json.put(JSON_ID, id.toString());
        json.put(JSON_WORD, word);
        json.put(JSON_DEFINITION, definition);
      //  Log.v("JSON FLASHCARD: ",json.toString());
        return json;
    }

    @Override
    public String toString(){
        return word;
    }

    public UUID getId(){
        return id;
    }

    public static boolean showWord(){
        return showWord = true;
    };

    public static void showDef(){
        showDef = true;
    };

    public static boolean hideWord(){
        return showWord = false;
    };

    public static void hideDef(){
        showDef = false;
    };

    public String getWord(){
        return word;
    }

    public void setWord(String newWord){
        word = newWord;
    }

    public String getDefinition(){
        return definition;
    }

    public void setDefinition(String newDef){
        definition = newDef;
    }


}
