package com.example.mtokunag.flashcard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by mtokunag on 12/23/2015.
 */
public class FlashCardListActivity extends AppCompatActivity {
    Button read_swipe;
    Button read_click;
    ArrayList<FlashCard> flashCards;
    Subject sc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_list_activity);

        UUID subjectId = (UUID)getIntent().getSerializableExtra(MainActivity.SUBJECT_ID);
        sc = Cards.get(this).getSubject(subjectId);
        flashCards = Cards.get(this).getCards(sc);

        read_swipe = (Button)findViewById(R.id.start_reading_swipe);
        read_swipe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flashCards.size()>0) {
                    // make MainActivity.class instead if you want buttons instead of swiping
                    Intent i = new Intent(FlashCardListActivity.this, ReaderActivity.class);
                    i.putExtra(ReaderActivity.SUBJECT_ID, sc.getId());
                    i.putExtra(ReaderActivity.IS_RANDOM, false);
                    startActivityForResult(i, 0);
                }
            }
        });

        read_click = (Button)findViewById(R.id.start_reading_click);
        read_click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(flashCards.size()>0) {
                    // make MainActivity.class instead if you want buttons instead of swiping
                    Intent i = new Intent(FlashCardListActivity.this, MainActivity.class);
                    i.putExtra(ReaderActivity.SUBJECT_ID, sc.getId());
                    i.putExtra(ReaderActivity.IS_RANDOM, false);
                    startActivityForResult(i, 0);
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.flashcard_list_menu, menu);
        return true;
    }
}
