package com.example.mtokunag.flashcard;

import android.app.ListFragment;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.UUID;

/**
 * Created by mtokunag on 12/23/2015.
 */
public class FlashCardListFragment extends ListFragment{
    ArrayList<FlashCard> flashCards;
    Subject sc;

    @Override
    public void onStart() {
        super.onStart();
        getListView().setEmptyView(
                noItems(getResources().getString(R.string.empty_flashcards)));
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        UUID subjectId = (UUID)getActivity().getIntent().getSerializableExtra(ReaderActivity.SUBJECT_ID);
        sc = Cards.get(getActivity()).getSubject(subjectId);

        flashCards = Cards.get(getActivity()).getCards(sc);
        getActivity().setTitle(R.string.flashcard_list);

        refreshList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        ListView listView = (ListView)v.findViewById(android.R.id.list);

        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.flashcard_list_context, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    // delete events
                    case R.id.menu_item_delete_flashcard:
                        CardArrayAdapter adapter = (CardArrayAdapter) getListAdapter();
                        Cards cards = Cards.get(getActivity());
                        for (int i = adapter.getCount() - 1; i >= 0; i--) {
                            if (getListView().isItemChecked(i)) {
                                FlashCard dfCard = adapter.getItem(i);
                                cards.deleteCard(sc, dfCard);
                            }
                        }
                        Cards.get(getActivity()).saveCards();
                        mode.finish();
                        adapter.notifyDataSetChanged();
                        // reload events after deleting
                        flashCards = Cards.get(getActivity()).getCards(sc);
                        refreshList();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });

        return v;
    }

    public void refreshList(){
        CardArrayAdapter adapter = new CardArrayAdapter(flashCards);
        setListAdapter(adapter);
    }


    // Implement Comparator for Collections.sort
    private class FlashCardComparator implements Comparator<FlashCard>{
        @Override
        public int compare(FlashCard f1, FlashCard f2){
            if(f1.getWord()==null&& f2.getWord()!=null)
                return "".compareTo(f2.getWord());
            else if(f2.getWord()==null && f1.getWord()!=null)
                return f1.getWord().compareTo("");
            else if(f1.getWord()==null && f2.getWord()==null)
                return "".compareTo("");
            else
                return f1.getWord().compareTo(f2.getWord());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.new_menu_flashcard:
                FlashCard fc = new FlashCard();
                Cards.get(getActivity()).addCard(sc, fc);
                Intent i = new Intent(getActivity(), AddCards.class);
                i.putExtra(AddCards.EXTRA_CARD_ID, fc.getId());
                i.putExtra(MainActivity.SUBJECT_ID, sc.getId());
                startActivityForResult(i, 0);
                return true;
            case R.id.randomize_list:
                Collections.shuffle(flashCards);
                refreshList();
                return true;
            case R.id.alphabetize_list:
                Collections.sort(flashCards, new FlashCardComparator());
                refreshList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id){
        FlashCard fcard = ((CardArrayAdapter)getListAdapter()).getItem(position);

        // Start EventActivity
        Intent i = new Intent(getActivity(), AddCards.class);
        i.putExtra(MainActivity.SUBJECT_ID, sc.getId());
        i.putExtra(AddCards.EXTRA_CARD_ID, fcard.getId());
        startActivityForResult(i, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ((CardArrayAdapter)getListAdapter()).notifyDataSetChanged();
        flashCards = Cards.get(getActivity()).getCards(sc);
        CardArrayAdapter adapter = new CardArrayAdapter(flashCards);
        setListAdapter(adapter);
    }

    private class CardArrayAdapter extends ArrayAdapter<FlashCard> {

        public CardArrayAdapter( ArrayList<FlashCard> fc) {
            super(getActivity(), R.layout.card_list_fragment, fc);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            if(convertView==null){
                convertView = View.inflate(getActivity(), R.layout.list_flashcard_item, null);
            }

            FlashCard fc = getItem(position);

            //Log.v("GETVIEW:", "" + mCards.size() + mCards.get(0).toString());

            TextView tv1 = (TextView)convertView.findViewById(R.id.list_flashcard_word);
            TextView tv2 = (TextView)convertView.findViewById(R.id.list_flashcard_definition);

            tv1.setText(fc.getWord());
            tv2.setText(fc.getDefinition());

            return convertView;
        }
    }


    private TextView noItems(String text) {
        TextView emptyView = new TextView(getActivity());
        //Make sure you import android.widget.LinearLayout.LayoutParams;
        emptyView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        //Instead of passing resource id here I passed resolved color
        //That is, getResources().getColor((R.color.gray_dark))
        emptyView.setTextColor(Color.DKGRAY);
        emptyView.setText(text);
        emptyView.setTextSize(36);
        emptyView.setVisibility(View.GONE);
        emptyView.setGravity(Gravity.CENTER_VERTICAL
                | Gravity.CENTER_HORIZONTAL);

        //Add the view to the list view. This might be what you are missing
        ((ViewGroup) getListView().getParent()).addView(emptyView);

        return emptyView;
    }
}
