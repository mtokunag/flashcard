package com.example.mtokunag.flashcard;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {
    public static final String SUBJECT_ID="com.example.mtokunag.flashcard.subject_id";
    public static final String IS_RANDOM="com.example.mtokunag.flashcard.random";
    private ArrayList<FlashCard> flashCards;
    Subject sc;
    TextView word_text;
    TextView def_text;
    Button next_button;
    Button prev_button;
    Button show_def;
    Button show_word;
    int count;
    int visible_def;
    int visible_word;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UUID subjectId = (UUID)getIntent().getSerializableExtra(SUBJECT_ID);
        sc = Cards.get(this).getSubject(subjectId);
        flashCards = Cards.get(this).getCards(sc);

        boolean isRandom = (boolean)getIntent().getSerializableExtra(IS_RANDOM);
        if(isRandom){
            Collections.shuffle(flashCards);
        }

        //flashCards = Cards.get(this).getCards();
        count = 0;
        visible_def = 0;
        visible_word = 1;
        setContentView(R.layout.activity_main);


        word_text = (TextView)findViewById(R.id.word_text);
        setWord();

        def_text = (TextView)findViewById(R.id.definition_text);
        def_text.setVisibility(View.INVISIBLE);
        setDef();

        next_button = (Button)findViewById(R.id.next_button);
        next_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (flashCards.size() > 0) {
                    count = (count + 1) % flashCards.size();
                    word_text.setText(flashCards.get(count).getWord());
                    def_text.setText(flashCards.get(count).getDefinition());
                }
            }
        });
        prev_button = (Button)findViewById(R.id.prev_button);
        prev_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(flashCards.size()>0) {
                    count = (count - 1) % flashCards.size();
                    if(count==-1)
                        count = flashCards.size()-1;
                    word_text.setText(flashCards.get(count).getWord());
                    def_text.setText(flashCards.get(count).getDefinition());
                }
            }
        });
        show_def = (Button)findViewById(R.id.show_definition_button);
        show_def.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(visible_def==0) {
                    def_text.setVisibility(View.VISIBLE);
                    visible_def = 1;
                }
                else {
                    def_text.setVisibility(View.INVISIBLE);
                    visible_def = 0;
                }
            }
        });

        show_word = (Button)findViewById(R.id.show_word_button);
        show_word.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(visible_word==0) {
                    word_text.setVisibility(View.VISIBLE);
                    visible_word = 1;
                }
                else {
                    word_text.setVisibility(View.INVISIBLE);
                    visible_word = 0;
                }
            }
        });


    }

    public void setWord(){
        if(flashCards.size() ==0){
            word_text.setText("Use \"+\" in toolbar to get started");
        }
        else{
            word_text.setText(flashCards.get(count).getWord());
        }
    }

    public void setDef(){
        if(flashCards.size() ==0){
            def_text.setText("Use \"+\" in toolbar to get started");
        }
        else{
            def_text.setText(flashCards.get(count).getDefinition());
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        flashCards = Cards.get(this).getCards(sc);
        finish();
        startActivity(getIntent());
    }

}
