package com.example.mtokunag.flashcard;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by mtokunag on 12/29/2015.
 */
public class ReaderActivity extends FragmentActivity {
    public static final String SUBJECT_ID="com.example.mtokunag.flashcard.subject_id";
    public static final String IS_RANDOM="com.example.mtokunag.flashcard.random";
    private ViewPager mViewPager;
    Subject sc;
    private ArrayList<FlashCard> mflashcards;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        mViewPager = new ViewPager(this);
        mViewPager.setId(R.id.viewPager);
        setContentView(mViewPager);

        UUID subjectId = (UUID)getIntent().getSerializableExtra(SUBJECT_ID);
        sc = Cards.get(this).getSubject(subjectId);
        mflashcards = Cards.get(this).getCards(sc);

        FragmentManager fm = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentStatePagerAdapter(fm){
            @Override
            public int getCount(){
                return mflashcards.size();
            }

            @Override
            public Fragment getItem(int pos){
                FlashCard fc = mflashcards.get(pos);
                return ReaderFragment.newInstance(sc.getId(), fc.getId());
            }

            @Override
            public int getItemPosition(Object object){
                return POSITION_NONE;
            }
        });
    }


}
