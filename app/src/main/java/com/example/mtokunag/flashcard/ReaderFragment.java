package com.example.mtokunag.flashcard;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

/**
 * Created by mtokunag on 12/29/2015.
 */
public class ReaderFragment extends Fragment {
    public static final String FRAGMENT_ID_FC="com.example.mtokunag.flashcard.fragment_id_fc";
    public static final String FRAGMENT_ID_SC="com.example.mtokunag.flashcard.fragment_id_sc";
    public static final String SHOW_WORD="com.example.mtokunag.flashcard.show_word";
    public static final String SHOW_DEF="com.example.mtokunag.flashcard.show_def";
    private ArrayList<FlashCard> flashCards;

    FlashCard fc;
    Subject sc;
    TextView word_text;
    TextView def_text;
    Button show_def;
    Button show_word;
    Button flip;
    LinearLayout screen;
    int count;
    boolean visible_def;
    boolean visible_word;

    public static ReaderFragment newInstance(UUID subjectId, UUID flashcardId) {
        Bundle args = new Bundle();
        args.putSerializable(FRAGMENT_ID_FC, flashcardId);
        args.putSerializable(FRAGMENT_ID_SC, subjectId);

        ReaderFragment fragment = new ReaderFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UUID subjectId = (UUID)getArguments().getSerializable(FRAGMENT_ID_SC);
        UUID flashcardID = (UUID)getArguments().getSerializable(FRAGMENT_ID_FC);
        sc = Cards.get(getActivity()).getSubject(subjectId);
        fc = Cards.get(getActivity()).getCard(sc, flashcardID);

        visible_word = FlashCard.showWord;
        //visible_def = FlashCard.showDef;

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.reader_fragment,parent,false);


        word_text = (TextView)v.findViewById(R.id.word_text);
        word_text.setText(fc.getWord());
        word_text.setTextSize(50);
        word_text.setGravity(Gravity.CENTER_HORIZONTAL);

        screen = (LinearLayout)v.findViewById(R.id.linearlayout);
        screen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (visible_word) {
                    visible_word = FlashCard.hideWord();
                    word_text.setText(fc.getDefinition());
                    word_text.setTextSize(22);
                    word_text.setGravity(Gravity.START);
                } else {
                    visible_word = FlashCard.showWord();
                    word_text.setText(fc.getWord());
                    word_text.setTextSize(50);
                    word_text.setGravity(Gravity.CENTER_HORIZONTAL);
                }

            }
        });

       /* flip = (Button)v.findViewById(R.id.flip_button);
        flip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (visible_word) {
                    visible_word = FlashCard.hideWord();
                    word_text.setText(fc.getDefinition());
                    word_text.setTextSize(18);
                    word_text.setGravity(Gravity.START);
                } else {
                    visible_word = FlashCard.showWord();
                    word_text.setText(fc.getWord());
                    word_text.setTextSize(30);
                    word_text.setGravity(Gravity.CENTER_HORIZONTAL);
                }

            }
        });*/
        /*if(!visible_word)
        {
            word_text.setVisibility(View.INVISIBLE);
        }

        def_text = (TextView)v.findViewById(R.id.definition_text);
        def_text.setText(fc.getDefinition());
        if(!visible_def){
            def_text.setVisibility(View.INVISIBLE);
        }

        show_def = (Button)v.findViewById(R.id.show_definition_button);
        show_def.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                if(!visible_def) {
                    def_text.setVisibility(View.VISIBLE);
                    FlashCard.showDef();
                    visible_def = FlashCard.showDef;
                }
                else {
                    def_text.setVisibility(View.INVISIBLE);
                    FlashCard.hideDef();
                    visible_def = FlashCard.showDef;
                }

            }
        });

        show_word = (Button)v.findViewById(R.id.show_word_button);
        show_word.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!visible_word) {
                    word_text.setVisibility(View.VISIBLE);
                    FlashCard.showWord();
                    visible_word = FlashCard.showWord;
                } else {
                    word_text.setVisibility(View.INVISIBLE);
                    FlashCard.hideWord();
                    visible_word = FlashCard.showWord;
                }
            }
        });*/

        return v;
    }


}
