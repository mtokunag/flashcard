package com.example.mtokunag.flashcard;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by mtokunag on 12/16/2015.
 */
public class Subject {
    private static final String JSON_SID = "subject_id";
    private static final String JSON_SUBJECT = "subject";
    private static final String JSON_FLASHCARDS = "flashcards";
    private String subject;
    private ArrayList<FlashCard> cards;
    private UUID id;

    public Subject(){
        id = UUID.randomUUID();
    }

    public Subject(String s, ArrayList<FlashCard> fc){
        id = UUID.randomUUID();
        subject = s;
        cards = fc;
    }

    public Subject(JSONObject json) throws JSONException {
       // Log.v("JSON SUBJECT CONST: ", json.toString());
        id = UUID.fromString(json.getString(JSON_SID));
        if(json.has(JSON_SUBJECT)){
            subject = json.getString(JSON_SUBJECT);
           // Log.v("JSON_SUBJECT: ", json.getString(JSON_SUBJECT));
        }
        if(json.has(JSON_FLASHCARDS)){
            //cards = FlashCard(json.getJSONObject(JSON_FLASHCARDS));
            cards = new ArrayList<FlashCard>();
            JSONArray json_array = json.getJSONArray(JSON_FLASHCARDS);
           // Log.v("JSON_FC_BEFORELOOP ", json.getJSONArray(JSON_FLASHCARDS).toString());
            for(int i=0; i<json_array.length(); i++){
               // Log.v("i: ", ""+i);
               // Log.v("JSON_FC_INLOOP: ", json_array.getJSONObject(i).toString());
               cards.add(new FlashCard(json_array.getJSONObject(i)));

            }

        }
    }

    public JSONObject toJSON() throws JSONException{
        JSONObject json = new JSONObject();
        json.put(JSON_SID, id.toString());
        json.put(JSON_SUBJECT, subject);
        //json.put(JSON_FLASHCARDS, cards);
        JSONArray jsonarray = new JSONArray();
        for(FlashCard fc: cards){
            jsonarray.put(fc.toJSON());
        }
        json.put(JSON_FLASHCARDS, jsonarray);
        //Log.v("JSON SUBJECT: ",json.toString());
        return json;
    }

    @Override
    public String toString(){
        if(subject!=null)
            return subject;
        else
            return "empty";
    }

    public UUID getId(){
        return id;
    }

    public String getSubject(){
        return subject;
    }

    public void setSubject(String s){
        subject = s;
    }

    // Grabs the ArrayList<FlashCard>
    public ArrayList<FlashCard> getFlashCards(){
        return cards;
    }

    public void setFlashCards(ArrayList<FlashCard> fc){
        cards = fc;
    }
}
