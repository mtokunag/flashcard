package com.example.mtokunag.flashcard;

import android.app.ListFragment;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.LinearLayout.LayoutParams;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by mtokunag on 12/23/2015.
 */
public class SubjectListFragment extends ListFragment {
    ArrayList<Subject> mCards;
    Subject sc;


    @Override
    public void onStart() {
        super.onStart();
        getListView().setEmptyView(
                noItems(getResources().getString(R.string.empty)));
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mCards = Cards.get(getActivity()).getAllCards();
        getActivity().setTitle(R.string.subject_list);

        refreshList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState){
        View v = super.onCreateView(inflater, parent, savedInstanceState);
        ListView listView = (ListView)v.findViewById(android.R.id.list);

        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {
            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.subject_list_context, menu);
                return true;
            }

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                switch (item.getItemId()) {
                    // delete events
                    case R.id.menu_item_delete_subject:
                        CardArrayAdapter adapter = (CardArrayAdapter) getListAdapter();
                        Cards cards = Cards.get(getActivity());
                        for (int i = adapter.getCount() - 1; i >= 0; i--) {
                            if (getListView().isItemChecked(i)) {
                                Subject sCard = adapter.getItem(i);
                                cards.deleteSubject(sCard);
                            }
                        }
                        Cards.get(getActivity()).saveCards();
                        mode.finish();
                        adapter.notifyDataSetChanged();
                        // reload events after deleting
                        mCards = Cards.get(getActivity()).getAllCards();
                        refreshList();
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {

            }
        });

        return v;
    }

    public void refreshList(){
        CardArrayAdapter adapter = new CardArrayAdapter(mCards);
        setListAdapter(adapter);
    }

    // Implement Comparator for Collections.sort
    private class SubjectComparator implements Comparator<Subject> {
        @Override
        public int compare(Subject s1, Subject s2){
            if(s1.getSubject()==null&& s2.getSubject()!=null)
                return "".compareTo(s2.getSubject());
            else if(s2.getSubject()==null && s1.getSubject()!=null)
                return s1.getSubject().compareTo("");
            else if(s1.getSubject()==null && s2.getSubject()==null)
                return "".compareTo("");
            else
                return s1.getSubject().compareTo(s2.getSubject());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case R.id.new_menu_item_new_card_set:
                Subject sc = new Subject();
                Cards.get(getActivity()).addSubjectCard(sc);
                Intent i = new Intent(getActivity(), AddSubject.class);
                i.putExtra(AddSubject.EXTRA_SUBJECT_ID, sc.getId());
                startActivityForResult(i,0);
                return true;
            case R.id.randomize_list:
                Collections.shuffle(mCards);
                refreshList();
                return true;
            case R.id.alphabetize_list:
                Collections.sort(mCards, new SubjectComparator());
                refreshList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id){
        Subject sc = ((CardArrayAdapter)getListAdapter()).getItem(position);

        // Start EventActivity
        Intent i = new Intent(getActivity(), FlashCardListActivity.class);
        i.putExtra(MainActivity.SUBJECT_ID, sc.getId());
        startActivityForResult(i, 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        ((CardArrayAdapter)getListAdapter()).notifyDataSetChanged();
        mCards = Cards.get(getActivity()).getAllCards();
    }



    private class CardArrayAdapter extends ArrayAdapter<Subject> {


        public CardArrayAdapter( ArrayList<Subject> sc) {
            super(getActivity(), R.layout.subject_list_fragment, sc);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent){

            if(convertView==null){
                convertView = View.inflate(getActivity(), R.layout.list_item, null);
            }


            sc = getItem(position);

           // Log.v("GETVIEW:",""+mCards.size()+mCards.get(0).toString());

            TextView tv1 = (TextView)convertView.findViewById(R.id.list_card_subject);
            TextView tv2 = (TextView)convertView.findViewById(R.id.list_num_cards);
            ImageButton edit = (ImageButton)convertView.findViewById(R.id.subject_edit);
            edit.setTag(position);
            //Button edit = (Button)convertView.findViewById(R.id.subject_edit);

            tv1.setText(sc.getSubject());
            tv2.setText(Integer.toString(sc.getFlashCards().size()));
            edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Integer pos = (Integer)v.getTag();
                    sc = getItem(pos);
                    Intent i = new Intent(getActivity(), AddSubject.class);
                    i.putExtra(AddSubject.EXTRA_SUBJECT_ID, sc.getId());
                    startActivityForResult(i,0);
                }
            });

            return convertView;
        }
    }


    private TextView noItems(String text) {
        TextView emptyView = new TextView(getActivity());
        //Make sure you import android.widget.LinearLayout.LayoutParams;
        emptyView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
        //Instead of passing resource id here I passed resolved color
        //That is, getResources().getColor((R.color.gray_dark))
        emptyView.setTextColor(Color.DKGRAY);
        emptyView.setText(text);
        emptyView.setTextSize(36);
        emptyView.setVisibility(View.GONE);
        emptyView.setGravity(Gravity.CENTER_VERTICAL
                | Gravity.CENTER_HORIZONTAL);

        //Add the view to the list view. This might be what you are missing
        ((ViewGroup) getListView().getParent()).addView(emptyView);

        return emptyView;
    }
}
